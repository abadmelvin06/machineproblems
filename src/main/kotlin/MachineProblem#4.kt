import kotlin.concurrent.fixedRateTimer

fun main() {
    //set of questions
    val questions = setOf(
        "1. What is the shape of the ball? \nA.Square  B.Round  C.Triangle",
        "\n2. Time of the shot clock? \nA.10 seconds  B.16 seconds  C.24 seconds",
        "\n3. What is the height of the rim? \nA.10 feet  B.12 feet  C.6 feet",
        "\n4. How many quarters in a game? \nA.Four  B.Five  C.Three",
        "\n5. Time for each quarters? \nA.6 minutes  B.10 minutes  C.12 minutes",
        "\n6. How many referee needed? \nA.Two  B.One  C.Three",
        "\n7. How many players on each team to be on court? \nA.Five  B.Four  C.Six",
        "\n8. What is the foul limit for each player? \nA.Four  B.Six  C.Five",
        "\n9. How many timeouts for each team? \nA.Three B.Ten C.Seven",
        "\n10. How many free throws on a regular foul? \nA.Two B.One C.Three"
    )

    //set of answers
    val answers = mapOf<Any, Any>(
        1 to "B",
        2 to "C",
        3 to "A",
        4 to "A",
        5 to "C",
        6 to "C",
        7 to "A",
        8 to "B",
        9 to "B",
        10 to "A"
    )

    var myAns = ""
    var n = 0
    var correctAns = 0
    var incorrectAns = 0

    questions.forEach() {
        println(it)
        n +=1
        do {
            println("Enter Valid Answer(A, B, C):")
            myAns = readLine()!!
        } while (myAns != "A" && myAns != "B" && myAns != "C")
        if(myAns == answers.getValue(n)){
            println("***** Correct *****")
            correctAns += 1
        }else{
            println("*** Correct answer is ${answers.getValue(n).toString().uppercase()} ***")
            incorrectAns +=1
        }

    }
println("\nNumber of correct answer: $correctAns \nNumber of incorrect answer: $incorrectAns")

}