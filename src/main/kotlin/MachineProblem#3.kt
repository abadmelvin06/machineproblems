import kotlin.system.exitProcess

fun main(){


    val directory = mutableMapOf<String, String>(
        "james" to "0441458",
        "carlo" to "0441845",
        "patricio" to "0448874",
        "jessie" to "0478561",
        "cris" to "0485125",
        "jenny" to "08424580",
        "aj" to "1431548",
        "ron" to "0485481",
        "jek" to "0785153",
        "kev" to "1344879"
    )
    var myName = ""
    var myPhone = ""

    do {
        try {
            println("What is your name?:")
            myName = readLine()!!.lowercase()
            if(myName == "quit"){
                exitProcess(0)
            }else {
                println("Your phone number is: ${directory.getValue(myName)}")
            }

        } catch (e: NoSuchElementException){
            println("You're not on the list, what is your phone number?:")
            myPhone = readLine()!!
            println("Saved number!")
            directory[myName] = myPhone
        }
    }while (directory.count() < 12)


println("*****List are full, cannot proceed!*****\n")
    println(directory)
}