//fun main(){
//    fun String.alphabetized() = String(toCharArray().apply { sort() })
//
//    println("Enter a any letters or word:")
//    val myWord = readLine()!!
//    if(myWord.count() <= 15){
//        if(myWord.count() % 2 == 0){
//            val reverseWord = myWord.reversed()
//            println(reverseWord)
//        }else{
//            println(myWord.alphabetized())
//        }
//    }else {
//        println("Only accepts less than 15 characters!")
//    }
//
//
//

import java.lang.IllegalArgumentException

fun main(){
//    println(myString("aksdiwhajdnsbajwqew"))
//    println(myString("edsFswz"))
//    print(myString("ajgz"))

    println("Enter a word:")
    (readLine()?.let { myString(it) })
}

    fun myString(myWord: String): Unit{
        if(myWord.count() >15){
            throw IllegalArgumentException("Only accepts less than 15 characters!")
        }
        return if(myWord.count() % 2 == 0){
            println(myWord.reversed())
        }else{
            println(myWord.alphabetized())
        }
    }
fun String.alphabetized() = String(toCharArray().apply { sort() })


