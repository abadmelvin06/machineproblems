//fun main(){
//
//    //SOLUTION NUMBER 1
////colors of rainbows = red orange yellow green blue indigo violet
//
//    val colorOfRainbows = mutableMapOf<String, Int>(
//        "red" to 0,
//        "orange" to 0,
//        "yellow" to 0,
//        "green" to 0,
//        "blue" to 0,
//        "indigo" to 0,
//        "violet" to 0,
//        "other" to 0
//    )
//    var counter = 0
//
//        //edit list of colors here!//
//    val rainbows = mutableListOf("red", "red", "blue", "indigo", "black", "pink", "green", "green", "green")
////    do{
//
//       if(rainbows[counter] == "red") {
//           colorOfRainbows["red"] = (colorOfRainbows.getValue("red") + 1)
//       }else if(rainbows[counter] == "orange") {
//           colorOfRainbows["orange"] = (colorOfRainbows.getValue("orange") + 1)
//       }else if(rainbows[counter] == "yellow") {
//           colorOfRainbows["yellow"] = (colorOfRainbows.getValue("yellow") + 1)
//       }else if(rainbows[counter] == "green") {
//           colorOfRainbows["green"] = (colorOfRainbows.getValue("green") + 1)
//       }else if(rainbows[counter] == "blue") {
//           colorOfRainbows["blue"] = (colorOfRainbows.getValue("blue") + 1)
//       }else if(rainbows[counter] == "indigo") {
//           colorOfRainbows["indigo"] = (colorOfRainbows.getValue("indigo") + 1)
//       }else if(rainbows[counter] == "violet") {
//           colorOfRainbows["violet"] = (colorOfRainbows.getValue("violet") + 1)
//       }else
//           colorOfRainbows["other"] = (colorOfRainbows.getValue("other")+1)
//        counter += 1
//  }while(counter <= rainbows.lastIndex)
//
//  println(colorOfRainbows)

//SOLUTION NUMBER 2
    fun main() {
//colors of rainbows = red orange yellow green blue indigo violet
println(checkRainbow(rainbows = mutableListOf("red", "red", "blue", "violet", "black", "pink", "green", "green", "green")))

}
val colorOfRainbows = setOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")
    fun checkRainbow(rainbows: List<String>): Unit {
        val difference1 = rainbows.minus(colorOfRainbows)
        val difference2 = rainbows.minus(difference1.toSet())
        val frequency = difference2.groupingBy { it }.eachCount().toMutableMap()
        frequency["others"] = difference1.count()
        println(frequency)
    }

